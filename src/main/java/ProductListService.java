
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Patcharawalai
 */
public class ProductListService {

    private static ArrayList<Product> productList = null;

    static {
        productList = new ArrayList<>();
        productList.add(new Product("1" , "Rice" , "easy" , "15.0" , "1"));
        productList.add(new Product("2" , "Sausage" , "Ceepee" , "35.0" , "1"));
        productList.add(new Product("3" , "Snack" , "ArhanYodkhun" , "10.5" , "2"));

    }

    public static boolean addProduct(Product Product) {
        productList.add(Product);
        return true;
    }

    public static boolean delProduct(Product Product) {
        productList.remove(Product);
        return true;
    }
    
    public static boolean delProduct(int index) {
        productList.remove(index);
        return true;
    }

    public static ArrayList<Product> getProducts() {
        return productList;
    }
    
    public static Product getProduct(int index){
        return productList.get(index);
    }
    
    public static boolean updateProduct(int index,Product Product) {
        productList.set(index, Product);
        return true;
    }
    
    
     public static void save() {
         
     }
     
     public static void load() {
         
     }
     
}
