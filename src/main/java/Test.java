/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Patcharawalai
 */
public class Test {

    public static void main(String[] args) {
        System.out.println(ProductListService.getProducts());
        ProductListService.addProduct(new Product("ID10", "Name10", "Brand10", "Price10", "Amount10"));
        System.out.println(ProductListService.getProducts());
        Product updateProduct = new Product("ID10+", "Name10+", "Brand10+", "Price10+", "Amount10+");
        ProductListService.updateProduct(3, updateProduct);
        System.out.println(ProductListService.getProducts());
        ProductListService.delProduct(updateProduct);
        System.out.println(ProductListService.getProducts());
        ProductListService.delProduct(1);
    }
}
